# Build tools

A collection of build tools I find useful. Feel free to copy them into your projects.
To run the tests, simply execute ``pytest`` at the top level of this repository.
