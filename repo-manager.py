#!/usr/bin/env python3

################################################################################
## Script to automate package upload and cleanup into RPM repositories.       ##
## Author: Georgios Bitzes - CERN                                             ##
## https://gitlab.cern.ch/gbitzes/build-tools                                 ##
##                                                                            ##
## Copyright (C) 2018 CERN/Switzerland                                        ##
## This program is free software: you can redistribute it and/or modify       ##
## it under the terms of the GNU General Public License as published by       ##
## the Free Software Foundation, either version 3 of the License, or          ##
## (at your option) any later version.                                        ##
##                                                                            ##
## This program is distributed in the hope that it will be useful,            ##
## but WITHOUT ANY WARRANTY; without even the implied warranty of             ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              ##
## GNU General Public License for more details.                               ##
##                                                                            ##
## You should have received a copy of the GNU General Public License          ##
## along with this program.  If not, see <http://www.gnu.org/licenses/>.      ##
################################################################################

import os, subprocess, sys, inspect, argparse, re, shutil, errno

DRY_RUN = False
NO_CREATE_REPO = False

def sh(cmd):
    # poor man's subprocess.check_output, not supported on SL6
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, stderr=subprocess.STDOUT)
    output, unused_err = process.communicate()
    retcode = process.poll()

    if retcode:
        raise Exception("Command {0} exited with code {1}".format(cmd, retcode))

    return output

def ensure_valid_choice(parser, choice, text, available):
    if choice:
        if type(choice) == str: choice = [choice]

        for item in choice:
            if item not in available:
                parser.error("unrecognized {0}: '{1}'. Available choices: {2}".format(text, item, available))

def add_dependency(parser, when_present, dependency):
    if hasattr(parser, when_present):
        if not hasattr(parser, dependency):
            parser.error("argument --{0} is required when --{1} is present".format(dependency, when_present))

def declare_required(parser, args, choice):
    if not hasattr(args, choice):
        parser.error("argument --{0} is required".format(choice))

def bailout(msg):
    raise ValueError(msg)

class PackageType:
    Binary, NoArch, Source = range(1, 4)

class PackageArchitecture:
    X86_64, X86, NotApplicable = range(1, 4)

class PackageNameParser(object):
    @staticmethod
    def determinePackageType(filename):
        if filename.endswith(".src.rpm"):
            return PackageType.Source
        if filename.endswith(".noarch.rpm"):
            return PackageType.NoArch
        if filename.endswith(".rpm"):
            return PackageType.Binary

        raise ValueError("unable to determine package type for {0}".format(filename))

    @staticmethod
    def stripPackageExtension(filename):
        packageType = PackageNameParser.determinePackageType(filename)
        if packageType == PackageType.Binary:
            return ".".join(filename.split(".")[0:-1])
        if packageType == PackageType.Source or packageType == PackageType.NoArch:
            return ".".join(filename.split(".")[0:-2])

        raise ValueError("unable to strip package extension for {0}".format(filename))

    @staticmethod
    def determinePackageArchitecture(filename):
        packageType = PackageNameParser.determinePackageType(filename)
        stripped = PackageNameParser.stripPackageExtension(filename)

        if packageType == PackageType.NoArch or packageType == PackageType.Source:
            return PackageArchitecture.NotApplicable

        if stripped.endswith(".x86_64"):
            return PackageArchitecture.X86_64

        if stripped.endswith(".i386"):
            return PackageArchitecture.X86

        raise ValueError("unable to determine package architecture for {0}".format(filename))

    @staticmethod
    def stripPackageExtensionAndArchitecture(filename):
        packageArch = PackageNameParser.determinePackageArchitecture(filename)
        stripped = PackageNameParser.stripPackageExtension(filename)

        if packageArch == PackageArchitecture.NotApplicable:
            return stripped

        return ".".join(stripped.split(".")[0:-1])

    @staticmethod
    def determinePackagePlatform(filename):
        stripped = PackageNameParser.stripPackageExtensionAndArchitecture(filename)

        split = stripped.split(".")
        if split[-1] == "cern":
            return split[-2]
        return split[-1]

    @staticmethod
    def determinePackageNameWithVersionAndRelease(filename):
        stripped = PackageNameParser.stripPackageExtensionAndArchitecture(filename)

        split = stripped.split(".")
        if split[-1] == "cern":
            return ".".join(stripped.split(".")[0:-2])
        return ".".join(stripped.split(".")[0:-1])

    @staticmethod
    def determinePackageVersion(filename):
        stripped = PackageNameParser.determinePackageNameWithVersionAndRelease(filename)
        return stripped.split("-")[-2]

    @staticmethod
    def determinePackageRelease(filename):
        stripped = PackageNameParser.determinePackageNameWithVersionAndRelease(filename)
        return stripped.split("-")[-1]

    @staticmethod
    def determinePackageName(filename):
        stripped = PackageNameParser.determinePackageNameWithVersionAndRelease(filename)
        return "-".join(stripped.split("-")[0:-2])

class PackageDescriptor(object):
    def __init__(self, path):
        self.path = path
        self.filename = os.path.basename(self.path)
        self.directory = os.path.dirname(self.path)
        self.packageType = PackageNameParser.determinePackageType(self.filename)
        self.packageArchitecture = PackageNameParser.determinePackageArchitecture(self.filename)
        self.packagePlatform = PackageNameParser.determinePackagePlatform(self.filename)
        self.packageNameWithVersionAndRelease = PackageNameParser.determinePackageNameWithVersionAndRelease(self.filename)
        self.packageVersion = PackageNameParser.determinePackageVersion(self.filename)
        self.packageRelease = PackageNameParser.determinePackageRelease(self.filename)
        self.packageName = PackageNameParser.determinePackageName(self.filename)

    def getFilename(self):
        return self.filename

    def getDirectory(self):
        return self.directory

    def getFullPath(self):
        return self.path

    def getPackageType(self):
        return self.packageType

    def getPackageArchitecture(self):
        return self.packageArchitecture

    def getPackagePlatform(self):
        return self.packagePlatform

    def getPackageNameWithVersionAndRelease(self):
        return self.packageNameWithVersionAndRelease

    def getPackageVersion(self):
        return self.packageVersion

    def getPackageRelease(self):
        return self.packageRelease

    def getPackageName(self):
        return self.packageName

def construct_location(platform, arch, filename):
    return "{0}/{1}/{2}".format(platform, arch, filename)

def is_tag(ref):
    return (re.compile("""^(v?)(\d+)\.(\d+)\.(\d+)$""").match(ref) != None or
           re.compile("""^(v?)(\d+)\.(\d+)$""").match(ref) != None)

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def createrepo(repo):
    print("-- Running createrepo on {0}".format(repo))
    if NO_CREATE_REPO: return
    sh("createrepo -q {0}".format(repo))

def copy_to_repo(source, repo):
    print("-- Copying {0} to {1}".format(source, repo))
    if DRY_RUN: return

    mkdir_p(repo)
    shutil.copyfile(source, "{0}/{1}".format(repo, os.path.basename(source)))

class Repository(object):
    def __init__(self, base):
        self.base = base
        if not os.path.isdir(self.base):
            bailout("Not a directory: {0}".format(self.base))

    def store(self, ref, packages):
        platforms = set([x.platform for x in packages])
        if len(platforms) != 1:
            raise ValueError("Cannot mix packages of different platforms in the same invocation: {0}".format(list(platforms)))

        archs = set([x.arch for x in packages])
        archs.remove(None)
        if len(archs) != 1:
            raise ValueError("Cannot mix packages of different architectures in the same invocation: {0}".format(list(archs)))

        tag = is_tag(ref)

        base = "{0}/{1}".format(self.base, ref)
        if tag: base = "{0}/tag".format(self.base)
        base += "/" + list(platforms)[0]

        for package in packages:
            repo = "{0}/{1}".format(base, list(archs)[0])
            if package.type == PackageType.Source:
                repo = "{0}/SRPMS".format(base)

            copy_to_repo(package.path, repo)
            createrepo(repo)

def declare_incompatible_options(parser, option, group):
    if option not in sys.argv: return

    for item in group:
        if item in sys.argv:
            parser.error("argument {0} is incompatible with argument {1}".format(option, item))

def parseargs():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description="An opinionated yum repository manager.\n")

    parser.add_argument('--base', type=str, required=True, help="The base directory for your project.")
    parser.add_argument('--action', type=str, required=True, help="The action to perform. Choices: ['add', 'cleanup']")
    parser.add_argument('--dry-run', action="store_true", help="If set, don't actually change any files, just show what would happen if ran.")
    parser.add_argument('--no-create-repo', action="store_true", help="If set, don't run createrepo at the end.")
    parser.set_defaults(dry_run=False, no_create_repo=False)

    group = parser.add_argument_group('add options')
    group.add_argument('--ref', type=str, required=True, help="The branch or tag that is being built. Tag names must match 'x.y' or 'x.y.z' (may be prepended by 'v')")
    group.add_argument('--packages', type=str, nargs='+', help="The list of packages to add")

    group = parser.add_argument_group('cleanup options')
    group.add_argument('--keep-last-days', type=int, help="How many days worth of RPMs to keep. (only affects branches)")

    args = parser.parse_args()

    ensure_valid_choice(parser, args.action, "action", ["add", "cleanup"])
    declare_incompatible_options(parser, "--no-create-repo", ["--dry-run"])

    if args.action == "add":
        declare_required(parser, args, "ref")
        declare_required(parser, args, "packages")

    if args.action == "cleanup":
        declare_required(parser, args, "keep-last-days")
        bailout("NYI")

    if args.ref == "tags" or args.ref == "tag":
        bailout("A branch named '{0}'? Really?".format(args.ref))

    global DRY_RUN
    global NO_CREATE_REPO
    if args.dry_run:
        DRY_RUN = True
        NO_CREATE_REPO = True

    if args.no_create_repo:
        NO_CREATE_REPO = True

    return args

def main():
    args = parseargs()

    repository = Repository(args.base)
    packages = [Package(x) for x in args.packages]

    repository.store(args.ref, packages)

if __name__ == '__main__':
    main()
